<?php /*Arquivo que controla a parte public.*/ ?>

<?php 
	require_once("../includes/session.php");
	require_once("../includes/db_connection.php");
	require_once("../includes/functions.php");
 ?>

 <?php 
 	$layout_context = "public";
 	include("../includes/layout/header.php"); 
 ?>

 <?php find_select_page(true); ?>

<div id="main">
	<div id="navigation">
		<?php echo navigation($current_subject, $current_page); ?><br>
	</div>
	<div id="page">
		<?php 
			if($current_page){
				echo "<h2>" . htmlentities($current_page["menu_name"]) . "</h2>";
				echo "<div class=\"view-content\">" . nl2br(htmlentities($current_page["content"])) . "</div><br>"; 
				//Em content, se pularmos linha não será respeitado por causa do htmlentities, usando a função nl2br() mostra o pulo das linhas, neste caso só estamos usando em public. (cap.18 - video 04)
			}else{
				echo "<p>Welcome!</>";
			}
		 ?>
	</div>
</div>

<?php 
	include("../includes/layout/footer.php");
 ?>