<?php 
	require_once("../includes/session.php");
	require_once("../includes/db_connection.php");
	require_once("../includes/functions.php");
	require_once("../includes/validation_function.php");

	//Confirma se o usuário é admin
	confirm_logged_in();
 ?>

 <?php 
 	if(isset($_POST["submit"])){
 		//Process the form
 		$menu_name = $_POST["menu_name"];
 		$position = (int) $_POST["position"];
 		$visible = (int) $_POST["visible"];

 		//Validations
 		$require_fields = array("menu_name", "position", "visible");
 		validate_presences($require_fields);

 		$fields_with_max_lengths = array("menu_name"=> 30);
 		validate_max_lengths($fields_with_max_lengths);

 		//Se o array associativo $errors não estiver vazio, é porque teve erro(s)
 		if(!empty($errors)){
 			$_SESSION["errors"] = $errors;
 			redirect_to("new_subject.php");
 		}

 		$menu_name = mysqli_real_escape_string($connection, $menu_name);
 		$query = "insert into subjects (menu_name, position, visible) values ('{$menu_name}' , {$position}, {$visible})";
 		$result = mysqli_query($connection, $query);

 		if($result){
 			//Success
 			$_SESSION["message"] = "Subject created."; 
 			redirect_to("manage_content.php");
 		}else{
 			//Failure
 			$_SESSION["message"] = "Subject created failed.";
 			redirect_to("new_subject.php");
 		}
 	}else{
 		//This is probably GET request
 		redirect_to("new_subject.php");
 	}

  ?>

 <?php 
 	//5. Close database connection
 	if(isset($connection))
 		mysqli_close($connection);
 ?>