<?php 
	require_once("../includes/session.php");
	require_once("../includes/db_connection.php");
	require_once("../includes/functions.php");

	//Confirma se o usuário é admin
	confirm_logged_in();
 ?>

 <?php 
 	$layout_context = "admin";
 	include("../includes/layout/header.php"); 
 ?>

 <?php find_select_page(); ?>

<div id="main">
	<div id="navigation">
		<br>
		<a href="admin.php">Main menu</a><br>
		<?php echo navigation($current_subject, $current_page, false); ?><br>
		<a href="new_subject.php">+ Add a subject</a><br>
		<a href="new_page.php">+ Add a page</a>
	</div>
	<div id="page">
		<?php echo message(); ?>
		<h2>Manage Content</h2>
		<?php 
			if($current_subject){
				echo "<h3>Subject: " . htmlentities($current_subject["menu_name"]) . "</h3>";
				echo "<h3>Position: " . $current_subject["position"] . "</h3>";
				$visible_subject = $current_subject["visible"] == 1 ? 'Yes' : 'No';
				echo "<h3>Visible: " . $visible_subject . "</h3><br>";
			}elseif($current_page){
				echo "<h3>Page: " . htmlentities($current_page["menu_name"]) . "</h3>";
				echo "<h3>Position: " . $current_page["position"] . "</h3>";
				$visible_page = $current_page["visible"] == 1 ? 'Yes' : 'No';
				echo "<h3>Visible: " . $visible_page . "</h3>";
				echo "<h3>Content:</h3>";
				echo "<div class=\"view-content\">" . htmlentities($current_page["content"]) . "</div><br>"; 
			}else{
				echo "Please select a subject or page<br>";
			}
		 ?>

		 <?php 
		 	if($current_subject){
		 		echo "<a href=edit_subject.php?subject=" . urlencode($current_subject["id"]) . ">Edit Subject</a>";
		 	}elseif($current_page){
		 		echo "<a href=edit_page?page=" . urlencode($current_page["id"]) . ">Edit Page</a>";
		 	}
		  ?>
	</div>
</div>

<?php 
	include("../includes/layout/footer.php");
 ?>