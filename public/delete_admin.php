<?php 
	require_once("../includes/session.php");
	require_once("../includes/db_connection.php");
	require_once("../includes/functions.php");

	//Confirma se o usuário é admin
	confirm_logged_in();
?>

<?php 
	$admin = find_admin_by_id($_GET["id"]);
	if(!$admin)
		redirect_to("manage_admins.php");

	$id = $admin["id"];
	$query = "delete from admins where id = {$id} limit 1";
	$result = mysqli_query($connection, $query);

	if($result && mysqli_affected_rows($connection) == 1){
		//Success
		$_SESSION["message"] = "Admin deleted";
		redirect_to("manage_admins.php");
	}else{
		//Failure
 		$_SESSION["message"] = "Admin delete failed.";
 		redirect_to("manage_admins.php");
	}
?>