<?php 
	require_once("../includes/session.php");
	require_once("../includes/db_connection.php");
	require_once("../includes/functions.php");

	//Confirma se o usuário é admin
	confirm_logged_in();
?>

<?php
	$layout_context = "admin"; 
	include("../includes/layout/header.php"); 
?>

<?php find_select_page();  ?>

<div id="main">
	<div id="navigation">
		<?php echo navigation($current_subject, $current_page); ?>
	</div>
	<div id="page">
		<?php echo message(); ?>
		<?php $errors = errors(); ?>
		<?php echo form_errors($errors); ?>
		<h2>Create Page</h2>
		<form action="create_page.php" method="post" autocomplete="off">
			<p>Page name:
				<input type="text" name="menu_name" value="">
			</p>
			<p>Subject:
				<select name="subject_id">
					<?php 
						$subject_set = find_all_subjects(false);
						while($subject = mysqli_fetch_assoc($subject_set)){
							echo "<option value=\"{$subject["id"]}\">{$subject["menu_name"]}</option>";
						}
					?>
				</select>						
			</p>
			<p>Position:
				<input type="number" name="position" min="1" max="100">
			</p>
			<p>Visible:
				<input type="radio" name="visible" value="0">No
				&nbsp;
				<input type="radio" name="visible" value="1">Yes
			</p>
			<p>Content:<br>
				<textarea name="content" rows="8" cols="50" maxlength="200"></textarea>
			</p>
			<input type="submit" name="submit" value="Create Page">
		</form>
		<br>
		<a href="manage_content.php">Cancel</a>
	</div>
</div>

<?php include("../includes/layout/footer.php"); ?>

