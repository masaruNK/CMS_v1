<?php 
	require_once("../includes/session.php");
	require_once("../includes/db_connection.php");
	require_once("../includes/functions.php");
	require_once "../includes/validation_function.php";
 ?>

 <?php 
 	$username = "";
 	if(isset($_POST["submit"])){
 		//Process the form

 		//validations
 		$required_fields = array("username", "password");
 		validate_presences($required_fields);

 		if(empty($errors)){
 			//Attempt login
 			$username = $_POST["username"];
 			$password = $_POST["password"];
 			$found_admin = attempt_login($username, $password);

 			if($found_admin){
 				//Success
 				//Mark user as logged in
 				$_SESSION["admin_id"] = $found_admin["id"];
 				$_SESSION["username"] = $found_admin["username"];
 				redirect_to("admin.php");
 			}else{
 				//Failure
 				$_SESSION["message"] = "Username/password not found.";
 			}
 		}
 	}else{
 		//This is probably a GET request
 	}
 ?>

 <?php 
 	$layout_context = "admin";
 	include "../includes/layout/header.php";
 ?>

 <div id="main">
 	<div id="navigation">
 		&nbsp;
 	</div>
 	<div id="page">
 		<?php echo message(); ?>
 		<?php echo form_errors($errors); ?>

 		<h2>Login</h2>
 		<form action="login.php" method="post" autocomplete="off">
 			<p>Username:
 				<input type="text" name="username" value="<?php echo htmlentities($username); ?>">
 			</p>
 			<p>Password:
 				<input type="password" name="password" value="">
 			</p>
 			<input type="submit" name="submit" value="Entrar">
 		</form>
 	</div>
 </div>

 <?php include "../includes/layout/footer.php"; ?>