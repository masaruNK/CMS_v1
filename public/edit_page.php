<?php 
	require_once("../includes/session.php");
	require_once("../includes/db_connection.php");
	require_once("../includes/functions.php");
	require_once("../includes/validation_function.php");

	//Confirma se o usuário é admin
	confirm_logged_in();
?>

<?php find_select_page(); ?>

<?php 
	if(!$current_page){
		//page ID was missing or invalid or page couldn't be
		//found in database
		redirect_to("manage_content.php");
	}
?>

<?php 
	if(isset($_POST["submit"])){
		//Validations
		$require_fields = array("subject_id", "menu_name", "position", "visible", "content");
		validate_presences($required_fields);

		$fields_with_max_lengths = array("menu_name" => 30, "content" => 200);
		validate_max_lengths($fields_with_max_lengths);

		if(empty($errors)){
			//Perform update
			$id = $current_page["id"];
			$subject_id = (int) $_POST["subject_id"];
			$menu_name = $_POST["menu_name"];
			$position = (int) $_POST["position"];
			$visible = (int) $_POST["visible"];
			$content = $_POST["content"];

			$menu_name = mysqli_real_escape_string($connection, $menu_name);
			$content = mysqli_real_escape_string($connection, $content);
			$query = "update pages set subject_id = {$subject_id}, menu_name = '{$menu_name}', position = {$position}, visible = {$visible}, content = '{$content}' where id = {$id} limit 1";
			$result = mysqli_query($connection, $query);

			if($result && mysqli_affected_rows($connection) >= 0){
				//Success
				$_SESSION["message"] = "Page update.";
				redirect_to("manage_content.php");
			}else{
				//Failure
				$message = "Page update failed.";
			}

		}
	}else{
		//This is probably GET request
	}
?>

<?php 
	$layout_context = "admin";
	include("../includes/layout/header.php"); 
?>

<div id="main">
	<div id="navigation">
		<?php echo navigation($current_subject, $current_page, false); ?>
	</div>
	<div id="page">
		<?php 
			//$message is just a variable, doesn't use the SESSION
			if(!empty($message)){
				echo "<div class=\"message\">" . htmlentities($message) . "</div>";
			}

			echo form_errors($errors);
		?>
		<h2>Edit Page: <?php echo htmlentities($current_page["menu_name"]); ?></h2>
		<form action="edit_page?page=<?php echo urlencode($current_page["id"]); ?>" method="post" autocomplete="off">
			<p>Page name:
				<input type="text" name="menu_name" value="<?php echo htmlentities($current_page["menu_name"]); ?>">
			</p>
			<p>Subject:
				<select name="subject_id">
					<?php 
						$subject_set = find_all_subjects(false);
						while($subject = mysqli_fetch_assoc($subject_set)){
							if($current_page["subject_id"] == $subject["id"]){
								echo "<option value=\"{$subject["id"]}\" selected>{$subject["menu_name"]}</option>";
							}else{
								echo "<option value=\"{$subject["id"]}\">{$subject["menu_name"]}</option>";
							}							
						}
					?>
				</select>
			</p>
			<p>Position:

				<input type="number" name="position" value="<?php echo $current_page["position"]; ?>">
				<?php
				/* 
					$page_set = find_pages_for_subjects($current_page["subject_id"]);
					$page_count = mysqli_num_rows($page_set);
					for($count = 1; $count <= $page_count; $count++){
						echo "<option value=\"{$count}\"";
						if($current_page["position"] == $count)
							echo "selected";
						echo ">{$count}</option>";
					}*/
				?>
			</p>
			<p>Visible:
				<input type="radio" name="visible" value="0" 
					<?php 
						if($current_page["visible"] == 0)
							echo "checked"; 
					?>
				> No
				&nbsp;
				<input type="radio" name="visible" value="1"
					<?php 
						if($current_page["visible"] == 1)
							echo "checked";
					?>
				> Yes
			</p>
			<p>Content:<br>
				<textarea name="content" rows="8" cols="50" maxlength="200"><?php echo $current_page["content"];?></textarea>
			</p>
			<input type="submit" name="submit" value="Edit Page">
		</form>
		<br>
		<a href="manage_content.php">Cancel</a>
		&nbsp;
		&nbsp;
		<a href="delete_page.php?page=<?php echo urlencode($current_page["id"]); ?>" onclick="return confirm('Are you sure?')">Delete page</a>
	</div>
</div>

<?php include("../includes/layout/footer.php"); ?>