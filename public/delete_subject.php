<?php 
	require_once("../includes/session.php");
	require_once("../includes/db_connection.php");
	require_once("../includes/functions.php");

	//Confirma se o usuário é admin
	confirm_logged_in();
?>

<?php 
	$current_subject = find_subject_by_id($_GET["subject"], false);
	if(!$current_subject){
		//subject ID was missing or invalid or subject couldn't be
		//found in database
		redirect_to("manage_content.php");
	}

	$pages_set = find_pages_for_subjects($current_subject["id"], false);
	if(mysqli_num_rows($pages_set) > 0){ //há pages que referenciam o subject, então não podemos simplesmente deletá-lo
		$_SESSION["message"] = "Can't delete a subject with pages.";
		//Volta para o subject que queríamos deletar
		redirect_to("manage_content.php?subject={$current_subject["id"]}");
	}

	$id = $current_subject["id"];
	$query = "delete from subjects where id = {$id} limit 1";
	$result = mysqli_query($connection, $query);

	if($result && mysqli_affected_rows($connection) == 1){
		//Success
		$_SESSION["message"] = "Subjected deleted.";
		redirect_to("manage_content.php");
	}else{
		//Failure
		$_SESSION["message"] = "Subject deletion failed.";
		//volta para página que queríamos deletar
		redirect_to("manage_content.php?subject={$id}"); 
	}
?>