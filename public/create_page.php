<?php 
	require_once("../includes/session.php");
	require_once("../includes/db_connection.php");
	require_once("../includes/functions.php");
	require_once("../includes/validation_function.php");

	//Confirma se o usuário é admin
	confirm_logged_in();
?>

<?php 
	if(isset($_POST["submit"])){
		//Process the form
		$menu_name = $_POST["menu_name"];
		$subject_id = (int) $_POST["subject_id"];
		$position = (int) $_POST["position"];
		$visible = (int) $_POST["visible"];
		$content = $_POST["content"];

		//Validations
		$require_fields = array("subject_id", "menu_name", "position", "visible", "content");
		validate_presences($require_fields);

		$fields_with_max_lengths = array("menu_name" => 30, "content" => 200);
		validate_max_lengths($fields_with_max_lengths);

		//Se o array associativo $errors não estiver vazio, é porque teve erro(s)
		if(!empty($errors)){
			$_SESSION["errors"] = $errors;
			redirect_to("new_page.php");
		}

		$menu_name = mysqli_real_escape_string($connection, $menu_name);
		$content = mysqli_real_escape_string($connection, $content);
		$query = "insert into pages (subject_id, menu_name, position, visible, content) values ({$subject_id}, '{$menu_name}', {$position}, {$visible}, '{$content}')";
		$result = mysqli_query($connection, $query);

		if($result){
			//Success
			$_SESSION["message"] = "Page created.";
			redirect_to("manage_content.php");
		}else{
			//Failure
			$_SESSION["message"] = "Page created failed.";
			redirect_to("new_page.php");
		}
	}else{
		//This is probably GET request
		redirect_to("new_page.php");
	}
?>

<?php 
	//5. Close database connection
	if(isset($connection))
		mysqli_close($connection);
?>

