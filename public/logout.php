<?php 
	require_once("../includes/session.php");
	require_once("../includes/functions.php");
?>

<?php 
	//Version 1: simple logout
	$_SESSION["admin_id"] = null;
	$_SESSION["username"] = null;
	redirect_to("login.php");
?>

<?php 
	//Version 2: destroy session
	//Assumes nothing else in session to keep
	
	/*
  	session_start();
  	$_SESSION = array(); //limpa todas as variáveis de $_SESSION
  	if(isset($_COOKIE[session_name()])){
  		setcookie(session_name(), '', time()-42000, '/');
  	}
  	session_destroy(); //destroi o arquivo SESSION no servidor
  	redirect_to("login.php");
  	*/
?>