<?php 
	//A variavel $layout_context será definida no arquivo que chamar header.php
	if(!isset($layout_context))
		$layout_context = "public";
?>	

<!DOCTYPE html>
<html>
<head>
	<title>Widget Corp <?php if($layout_context == "admin") echo "Admin"; ?></title>
	<link rel="stylesheet" type="text/css" href="./css/public.css">
</head>
<body>
	<div id="header">
		<h1>Widget Corp <?php if($layout_context == "admin") echo "Admin"; ?></h1>
	</div>
